<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCoors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('coors', function (Blueprint $table) {
        $table->increments('id');
        $table->double('lat', 15, 8);
        $table->double('lon', 15, 8);
        $table->integer('number_verhical');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('coors');
    }
}
