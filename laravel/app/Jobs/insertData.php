<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class insertData extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $users = array(
        ['lat' => 0.00001, 'lon' => 0.00001, 'number_verhical' => 100, 'created_at' => new \DateTime, 'updated_at' => new \DateTime]
      );

      \DB::table('coors')->insert($users);
      $this->info('Insert success!!!');
    }
}
