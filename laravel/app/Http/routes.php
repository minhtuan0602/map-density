<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/api/users/register', 'AuthController@register');
Route::post('/api/users/login', 'AuthController@login');
Route::post('/api/users/logout', 'AuthController@logout');

Route::post('/api/users/changepassword', 'AuthController@change_password');
Route::post('/api/users/forgotpassword', 'AuthController@forgot_password');

Route::get('/', function() {
  return "Minh Tuan";
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

