<?php

namespace App\Http\Controllers;

use App\User;
use Input;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
  public function register(Request $request) {
    $rules = array(
      'email_address' => 'required|email',
      'password' => 'required|min:6'
    );
    
    $validator = \Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
      return (new Response(json_encode(array('code' => 422, 'message' => 'Dữ liệu nhập không đúng định dạng yêu cầu', 
        'data' => null)), 200));
    }

    $input = Input::all();
    $user = User::where('email_address', '=', $input['email_address'])->first();
    if ($user) {
      return (new Response(json_encode(array('code' => 409, 'message' => 'Địa chỉ email đã được sử dụng bởi một tài khoản khác', 
        'data' => null)), 200));
    } else {
      $input['password'] = bcrypt($input['password']);
      User::create($input);
      return (new Response(json_encode(array('code' => 200, 'message' => null, 'data' => null)), 200));
    }    
  }


  public function login(Request $request) {
    $input = Input::all();
    $user = User::where('email_address', '=', $input['email_address'])->first();
    if ($user) {
      if (\Hash::check($input['password'], $user->password)) {
        $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+-="), 0, 100);
        $user->remember_token = $token;
        $user->update();
        return (new Response(json_encode(array('code' => 200, 'message' => null, 'data' => $token)), 200));
      }
    }
    return (new Response(json_encode(array('code' => 404, 'message' => 'Địa chỉ email hoặc mật khẩu không chính xác', 'data' => null)), 200));
  }


  public function logout(Request $request) {
    if (is_null($request->header('authentication'))) {
      return (new Response(json_encode(array('code' => 401, 'message' => null, 'data' => null)), 200));
    }

    $user = User::where('remember_token', '=', $request->header('authentication'))->first();
    if ($user) {
      $user->remember_token = null;
      $user->update();
      return (new Response(json_encode(array('code' => 200, 'message' => null, 'data' => null)), 200));
    } else {
      return (new Response(json_encode(array('code' => 401, 'message' => null, 'data' => null)), 200));
    }
  }


  public function change_password(Request $request) {
    if (is_null($request->header('authentication'))) {
      return (new Response(json_encode(array('code' => 401, 'message' => null, 'data' => null)), 200));
    }

    $user = User::where('remember_token', '=', $request->header('authentication'))->first();
    if ($user) {
      $rules = array(
        'new_password' => 'required|min:6'
      );
      $validator = \Validator::make(Input::all(), $rules);
      if ($validator->fails()) {
        return (new Response(json_encode(array('code' => 422, 'message' => 'Dữ liệu nhập không đúng định dạng yêu cầu', 
          'data' => null)), 200));
      }

      $input = Input::all();
      if (\Hash::check($input['current_password'], $user->password)) {
        $user->password = bcrypt($input['new_password']);
        $user->update();
        return (new Response(json_encode(array('code' => 200, 'message' => null, 'data' => null)), 200));
      } else {
        return (new Response(json_encode(array('code' => 404, 'message' => 'Mật khẩu không chính xác', 'data' => null)), 200));
      }
    } else {
      return (new Response(json_encode(array('code' => 401, 'message' => null, 'data' => null)), 200));
    }
  }


  public function forgot_password(Request $request) {

  }
}
