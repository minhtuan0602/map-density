<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $users = array(
        ['lat' => 0.00001, 'lon' => 0.00001, 'number_verhical' => 100, 'created_at' => new \DateTime, 'updated_at' => new \DateTime]
      );

      \DB::table('coors')->insert($users);
      $this->info('Insert success!!!');
    }
}
